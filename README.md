# Welcome to my Design Portfolio

My goal with this portfolio is to provide a window into some of the projects I've worked on and for you to leave with solid insights into my process and how I think and operate. Each project demonstrates different skills, such as executive stakeholder collaboration, cross-team collaboration, design critiques, growth experiments vs new feature implementation vs iterative work, etc.

👉 Head over to [issues](https://gitlab.com/tipyn/lk-design/-/issues) to take a look. 👈

## About me

Hi 👋 I'm Luca. I'm a curious and thoughtful Product Designer based in Austin, Texas. My interests in design primarily focus on ethical growth-hacking, form design, service design and accessibility. In recent years, I've spent a lot of my time improving and developing billing flows, trial experiences, user onboarding, and iterating on internal tools and processes for the companies I've worked with.

I see user experience and interface design as parts of a Venn diagram - and in the middle is product design. However, strategic thinking and product management are missing from this diagram - these are skills every designer should have. My experience as a product manager has been invaluable in balancing those two parts of the Venn diagram as I've shifted into product design. I've learned how to balance the needs of internal and external stakeholders, consider the business needs and the user and find a happy medium between the two. I understand and deeply value the importance of building cohesive and trusting relationships with your counterparts in Product and Engineering, which is critical to the team's success.

Outside of work, I love collecting vinyl and discovering new music, working on home projects such as building my own media server, playing the electric guitar, attempting to recreate the sounds of the 90s with various pedals and playing D&D and Magic the Gathering with my
 friends. I have a little long-haired Chihuahua/Pomeranian mix called Leo, and he's a little ray of sunshine in my life.

## How I  work

I love iteration. I love data, and I love making small but impactful changes. I learned to look for "the most boring solution" to a problem, and I'm always curious to dig deeper into the problem we're trying to solve versus taking something at face value. I want every change to be meaningful and intentional. The challenge here is balancing beautiful design and being able to see the bigger picture - iterations are great if you intentionally learn from them and don't use them as a crutch to lean on. It's easy to keep making small changes without thinking about the more significant impact of many changes over time - things can get messy, and understanding how to avoid UX debt at the same time as moving and learning (and failing) fast is crucial to building great products that customers love.

I have a holistic approach to design and high expectations of myself when I deliver work. Empathy is everything; humans and their experiences are central to my work, and I prioritise inclusive execution whenever possible. I cannot do my best work alone; therefore, collaborating well and working closely with my team is critical. I am known for my compassion and team-building abilities outside of my design skills, and  I thrive among open-minded, fun and emotionally intelligent people who put humans first. I love bringing people together is safe, creative spaces and believe that people come before process. I am passionate and energised by my design work, but it does not define me - my friends, family and community do.

If you have questions or are interested in working with me, shoot me an email at [hello@lucakisielius.com](mailto:hello@lucakisielius.com).
